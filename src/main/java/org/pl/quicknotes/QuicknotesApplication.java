package org.pl.quicknotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuicknotesApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuicknotesApplication.class, args);
	}

}
